part of 'configs.dart';

///string on apps code here
class ConstantString{
  static const URL_UPDATE_COVID = 'https://data.covid19.go.id/public/api/update.json';
  static const URL_PROV_COVID = 'https://data.covid19.go.id/public/api/prov.json';
  static const URL_FCM = 'https://fcm.googleapis.com/fcm/send';
  static const KEY_FCM = 'key=AAAA_p6g2rA:APA91bH_OQBLCqcpICKwn0I8Z6J-UHZyZFZPafy0z3-EKw_XEfsPJyl8tRTi7uB6ApNuQTPZutxJt104o60PzyeSvCo0UkY4gICN06Ori8R50uWFQ_osoRovNk6GH60G3QFKo_YgdzTh';

  static const ENABLED = 'enable';
  static const DISABLED = 'disable';
}